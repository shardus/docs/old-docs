# constructor

The constructor for Shardus expects a configuration object with settings that resemble the likes of [configuration](../configuration). If you don't provide a configuration object Shardus will use it's default configuration which you can also find [here](../configuration)

```ts
import shardus from 'shardus-global-server'
import config from './config'

const dapp = shardus(config)
```
