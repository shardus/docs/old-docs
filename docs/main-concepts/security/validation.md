# Validation

Validation is used to validate fields in account data to ensure that they meet the requirements for a specific transaction to occur. The most basic example of validation is checking if an account has enough balance to transfer coins to another account. Doing this with shardus is as easy as running a simple `"if" "else"` check. It is recommended to use a helper function for validating each transaction type individualy. For example

::: tip Validating account balance for "transfer" transaction.

In this example we will demonstrate how liberdus validates a "transfer" transaction. Below is a typescript interface defining the global network parameters the nodes use to validate transactions against. The only parameter we're concerned with is the `transactionFee`. This parameter is dynamic, meaning users can decide to change this during a voting cycle that occurs every few weeks. Due to the dynamic nature of this application, node's need access to this data on every transaction to perform validation. This means that one of the transaction keys needs to hold the account data for these parameters, and it needs to be submitted with the transaction. We will use `tx.network` to refer to the address of that account.

```ts

interface NetworkParameters {
    title: string
    description: string
    nodeRewardInterval: number
    nodeRewardAmount: number
    nodePenalty: number
    transactionFee: number
    stakeRequired: number
    maintenanceInterval: number
    maintenanceFee: number
    proposalFee: number
    devProposalFee: number
}

interface NetworkAccount {
    id: string
    current: NetworkParameters
    next: NetworkParameters | {}
    windows: Windows
    nextWindows: Windows | {}
    devWindows: DevWindows
    nextDevWindows: DevWindows | {}
    issue: number
    devIssue: number
    developerFund: DeveloperPayment[]
    nextDeveloperFund: DeveloperPayment[]
    hash: string
    timestamp: number
    snapshot?: object
}

interface UserAccount {
    id: string
    data: {
        balance: number
        toll: number
        chats: object
        friends: object
        stake?: number
        transactions: object[]
    }
    alias: string | null
    emailHash: string | null
    verified: string | boolean
    lastMaintenance: number
    claimedSnapshot: boolean
    timestamp: number
    hash: string
}

switch (tx.type) {
    case 'transfer': {
        const network: NetworkAccount = wrappedStates[tx.network].data
        const from: UserAccount = wrappedStates[tx.from].data
        const to: UserAccount = wrappedStates[tx.to].data
        if (tx.sign.owner !== tx.from) { // Validate owner of signature is the sending account
            response.reason = 'not signed by from account'
            return response
        }
        if (crypto.verifyObj(tx) === false) { // Validate the signature
            response.reason = 'incorrect signing'
            return response
        }
        if (from === undefined || from === null) { // Validate that an account exists for the sender
            response.reason = "from account doesn't exist"
            return response
        }
        if (to === undefined || to === null) { // validate that an account exists for the recipient
            response.reason = "To account doesn't exist"
            return response
        }
        if (from.data.balance < tx.amount + network.current.transactionFee) { // Validate that the sender has enough tokens to cover the transaction amount + the current network transaction fee
            response.reason = "from account doesn't have sufficient balance to cover the transaction"
            return response
        }
        response.success = true
        response.reason = 'This transaction is valid!'
        return response
    }
}
```

:::
