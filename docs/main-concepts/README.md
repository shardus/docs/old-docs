# Main Concepts

This section describes the main concepts of building decentralized applications on Shardus. Reading through these topics is highly recommended for anyone looking to build complex applications and learn a little bit more about how Shardus works.

---

::: tip Topics

* [Development](./development)
* [Security](./security)
* [Testing](./testing)
* [Debugging](./debugging)
* [Deployment](./deployment)
* [Considerations](./considerations)

:::

---
