# Initialization

## Cryptographic utilities

Shardus requires a few key steps to get up and running. One of them is to install the [`shardus-crypto-utils`](../../tools/crypto-utils.html) npm package.

```bash
npm install shardus-crypto-utils
# OR: yarn add shardus-crypto-utils
```

Before using the `crypto` package for hashing, or signing data, we need to initialize the module using the `init` function and pass in a 64 character hex key.

```ts
// Typescript
import * as crypto from 'shardus-crypto-utils';
// Javascript
const crypto = require('shardus-crypto-utils');

crypto.init('69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc');
```

::: tip
Learn more about how to use this package [here](../../tools/crypto-utils.html)
:::

## Monitor Client

The next things you should setup for Shardus are the [`monitor-server`](../../tools/monitor-server.html) and [`monitor-client`](../../tools/monitor-client.html). These are used to visually monitor the status of the nodes in the network. The vast majority of the time something goes wrong because of a bug, you'll be able to tell just by looking at the monitor client. This is a screenshot of what the monitor client looks like with 10 active nodes in the network who are all in sync.

![monitor-client](../../images/monitor_client.png)

::: warning tip
The [tools](../../tools/README.md) section of the docs has more detailed documentation on these packages.
:::

## Shardus CLI Tool

Lastly, you want to grab the [`shardus-cli-tool`](../../tools/shardus-cli-tool) which will provide you with various commands for helping you create, start, and stop networks.

::: danger tip
For the most part, you will be using the network tool provided in the shardus CLI tool, but there are a few other tools available as well. You can find more detailed documentation on the CLI tool [here](../../tools/shardus-cli-tool)
:::
