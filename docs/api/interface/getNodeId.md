# getNodeId

`getNodeId` is a function that returns the `nodeId` of whatever node calls this function. It's useful for getting the node's information by passing in the `nodeId` to [getNode](./getNode).
