# Security

Security is the most vital component of any decentralized application. The majority of value seen in the blockchain space stems from the understanding that blockchain is secure. While this is the idea, it is not the guarantee. Every application is free to implement its own security, therefore it is entirely possible to introduce bugs, or security flaws. Maintaining an eye on potential security flaws is required whenever introducing new transaction types to your network.

::: danger Validation
Validating transactions is the process of taking account data from the keys of the transaction and ensuring they meet certain conditions for that transaction.
[validation](./validation.html)
:::

::: tip Robustness
Robust transactions are highly critical transactions that need heavy validation measures to ensure security.
[robustness](./robustness.html)
:::
