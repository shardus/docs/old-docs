# put

`put` is the function that nodes will use to inject transactions into the network. It takes the transaction data as it's argument and Shardus handles all of the internal peer-to-peer gossip communication between the nodes to record the transaction.

::: tip
You need some sort of `/inject` endpoint for clients to send post requests to nodes. Always use [registerExternalPost](./registerExternalPost) to create this endpoint for your application.

```ts
// API
dapp.registerExternalPost(
  'inject',
  async (req, res): Promise<void> => {
    try {
      const result = dapp.put(req.body)
      res.json({ result })
    } catch (error) {
      dapp.log(error)
      res.json({ error })
    }
  },
)
```

:::
