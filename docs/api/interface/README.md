# Interface

The Shardus API provides you with a few essential methods required to configure a shardus network. You can see the required methods here:

---

* [setup](./setup)
* [applyResponseAddState](./applyResponseAddState)
* [constructor](./constructor)
* [createApplyResponse](./createApplyResponse)
* [getLocalOrRemoteAccount](./getLocalOrRemoteAccount)
* [put](./put)
* [registerExceptionHandler](./registerExceptionHandler)
* [registerExternalGet](./registerExternalGet)
* [registerExternalPost](./registerExternalPost)
* [start](./start)

---

::: tip
Although not required for a network to function, these functions can aid you when developing with more specific functionality in mind.

* [getClosestNodes](./getClosestNodes)
* [getLatestCycles](./getLatestCycles)
* [getNode](./getNode)
* [getNodeId](./getNodeId)
* [getRemoteAccount](./getRemoteAccount)
* [isFirstSeed](./isFirstSeed)
* [log](./log)
* [on](./on)
* [set](./set)

:::

---
