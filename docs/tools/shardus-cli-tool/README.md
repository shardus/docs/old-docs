# Shardus CLI Tool

The `shardus-cli` tool lets you:

- Create and manage local test networks running your app.
- Collect debug data from a running Shardus app network.

## Installation

```sh
npm i -g shardus
# OR yarn global add shardus
```

::: tip
The `shardus-cli-tool` also has autocomplete available to those of you who are using `zsh`.
In order to get the autocompletions working, just run:

```sh
# For zsh
source <(shardus completion zsh)

# or add it to your .zshrc to make it persist
echo "source <(shardus completion zsh)" >> ~/.zshrc \
&& source ~/.zshrc
```

:::

::: tip The sub modules included in the `shardus-cli`

- [shardus-network](./network)
- [shardus-scan](./scan)
- [shardus-debug](./debug)
- [shardus-deploy](./deploy)

:::
