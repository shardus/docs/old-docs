module.exports = {
  title: 'Shardus Documentation',
  description: 'Shardus Developer Documentation',
  base: '/docs/developer/',
  dest: 'public',
  themeConfig: {
    // repo: 'https://gitlab.com/shardus/docs/developer',
    // repoLabel: 'Contribute!',
    nextLinks: true,
    prevLinks: true,
    logo: '/assets/img/logo.png',
    docsDir: 'docs',
    editLinks: true,
    smoothScroll: true,
    editLinkText: 'Help us improve this page!',
    displayAllHeaders: false,
    searchPlaceholder: 'Search...',
    nav: [
      { text: 'SHARDUS SITE', link: 'https://shardus.com/' },
    ],
    sidebar: [
      {
        title: 'GETTING STARTED', // required
        path: '/', // optional, which should be a absolute
        collapsable: false, // optional, defaults to true
        sidebarDepth: 1 // optional, defaults to 1
      },
      {
        title: 'INSTALLATION', // required
        path: '/installation/', // optional, which should be a absolute
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: ['installation/setup.md', 'installation/quickstart.md', 'installation/windows.md'],
      },
      {
        title: 'EXAMPLE APPLICATIONS',
        path: '/examples/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: [
          { title: 'Hello Cloud', path: 'examples/hello-cloud-template.md', collapsable: true, sidebarDepth: 2 },
          { title: 'Tic Tac Toe', path: 'examples/tic-tac-toe-template.md', collapsable: true, sidebarDepth: 2 },
          { title: 'Coin App', path: 'examples/coin-app-template.md', collapsable: true, sidebarDepth: 2 },
          'examples/chat-app-template.md',
        ],
      },
      {
        title: 'TOOLS',
        path: '/tools/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: [
          {
            title: 'Shardus CLI Tool',
            path: '/tools/shardus-cli-tool/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: [
              'tools/shardus-cli-tool/network.md',
              'tools/shardus-cli-tool/scan.md',
              'tools/shardus-cli-tool/debug.md',
              'tools/shardus-cli-tool/deploy.md',
            ],
          },
          'tools/monitor-server.md',
          'tools/monitor-client.md',
          'tools/archive-server.md',
          'tools/crypto-utils.md',
          'tools/crypto-web-utils.md',
        ],
      },
      {
        title: 'MAIN CONCEPTS',
        path: '/main-concepts/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: [
          {
            title: 'Development',
            path: '/main-concepts/development/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: [
              'main-concepts/development/initialization.md',
              'main-concepts/development/accounts.md',
              {
                title: 'Transactions',
                path: '/main-concepts/development/transactions/',
                collapsable: true, // optional, defaults to true
                sidebarDepth: 1,
                children: [
                  'main-concepts/development/transactions/user-transactions.md',
                  'main-concepts/development/transactions/node-transactions.md',
                ],
              },
              'main-concepts/development/api.md',
              'main-concepts/development/cli.md',
              'main-concepts/development/frontend.md',
            ],
          },
          {
            title: 'Security',
            path: '/main-concepts/security/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: ['main-concepts/security/validation.md', 'main-concepts/security/robustness.md'],
          },
          {
            title: 'Testing',
            path: '/main-concepts/testing/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: ['main-concepts/testing/single-node.md', 'main-concepts/testing/network-of-nodes.md', 'main-concepts/testing/spamming.md'],
          },
          {
            title: 'Debugging',
            path: '/main-concepts/debugging/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: [
              'main-concepts/debugging/state.md',
              'main-concepts/debugging/transaction.md',
              'main-concepts/debugging/application.md',
              'main-concepts/debugging/common-errors.md',
            ],
          },
          {
            title: 'Deployment',
            path: '/main-concepts/deployment/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: ['main-concepts/deployment/remote-server.md', 'main-concepts/deployment/aws.md'],
          },
          {
            title: 'Considerations',
            path: '/main-concepts/considerations/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: ['main-concepts/considerations/incentivize-users.md', 'main-concepts/considerations/incentivize-miners.md'],
          },
        ],
      },
      {
        title: 'API REFERENCE',
        path: '/api/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: [
          {
            title: 'Configuration',
            path: '/api/configuration/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: ['api/configuration/server.md', 'api/configuration/logs.md', 'api/configuration/storage.md'],
          },
          {
            title: 'Interface',
            path: '/api/interface/',
            collapsable: true, // optional, defaults to true
            sidebarDepth: 1,
            children: [
              {
                title: 'setup',
                path: '/api/interface/setup/',
                collapsable: true, // optional, defaults to true
                sidebarDepth: 1,
                children: [
                  'api/interface/setup/apply.md',
                  'api/interface/setup/calculateAccountHash.md',
                  'api/interface/setup/canDebugDropTx.md',
                  'api/interface/setup/close.md',
                  'api/interface/setup/deleteAccountData.md',
                  'api/interface/setup/getAccountData.md',
                  'api/interface/setup/getAccountDataByList.md',
                  'api/interface/setup/getAccountDataByRange.md',
                  'api/interface/setup/getAccountDebugValue.md',
                  'api/interface/setup/crack.md',
                  'api/interface/setup/getRelevantData.md',
                  'api/interface/setup/resetAccountData.md',
                  'api/interface/setup/setAccountData.md',
                  'api/interface/setup/sync.md',
                  'api/interface/setup/updateAccountFull.md',
                  'api/interface/setup/updateAccountPartial.md',
                  'api/interface/setup/validate.md',
                ],
              },
              'api/interface/applyResponseAddState.md',
              'api/interface/constructor.md',
              'api/interface/createApplyResponse.md',
              'api/interface/createWrappedResponse.md',
              'api/interface/getClosestNodes.md',
              'api/interface/getLatestCycles.md',
              'api/interface/getLocalOrRemoteAccount.md',
              'api/interface/getNode.md',
              'api/interface/getNodeId.md',
              'api/interface/getRemoteAccount.md',
              'api/interface/isFirstSeed.md',
              'api/interface/log.md',
              'api/interface/on.md',
              'api/interface/put.md',
              'api/interface/registerExceptionHandler.md',
              'api/interface/registerExternalGet.md',
              'api/interface/registerExternalPost.md',
              'api/interface/set.md',
              'api/interface/setGlobal.md',
              'api/interface/start.md',
            ],
          },
        ],
      },
      /*
      This section is yet to be written.
      {
        title: 'ADVANCED GUIDES',
        path: '/advanced-guides/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: [
          'advanced-guides/advanced-configuration.md',
          'advanced-guides/rate-limiting.md',
          'advanced-guides/production.md',
          'advanced-guides/deploying-to-aws.md',
          'advanced-guides/automated-cicd.md',
          'advanced-guides/listing-on-dex.md',
          'advanced-guides/erc20-airdrop.md',
          'advanced-guides/cross-chain-atomic-swap.md',
        ],
      }, */
      {
        title: 'CONTRIBUTING',
        path: '/contributing/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: ['contributing/apply.md', 'contributing/support.md', 'contributing/feedback.md', 'contributing/issue.md'],
      },
      {
        title: 'FAQ',
        path: '/faq/', // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 1, // optional, defaults to 1
        children: ['faq/what-is-shardus.md', 'faq/why-use-shardus.md', 'faq/what-should-i-know.md', 'faq/performance.md'],
      },
    ],
  },
  plugins: [
    '@vuepress/nprogress',
    '@vuepress/plugin-active-header-links',
    '@vuepress/plugin-register-components',
    '@vuepress/plugin-search',
    'vuepress-plugin-container',
    'vuepress-plugin-smooth-scroll',
    '@vuepress/back-to-top',
  ],
};
