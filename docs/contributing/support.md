# Support The Project

If you aren't a developer, but still wish to support the project, there are several other ways you can help.

::: tip Other ways to help

* Design artistic assets
* Fix an issue on our [GitLab](https://gitlab.com/shardus) page
* Proofread the documentation, or our whitepaper
* Create a monitoring UI for [PM2](https://pm2.keymetrics.io/)
* Create debugging tools for pm2 instances
* Create scanning tools for sifting through log files
* Create clean, colorful CLI tools for interacting with [Liberdus](https://liberdus.com)

:::
