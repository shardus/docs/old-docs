- GUIDELINES -




1. Research
What do users need to know about your product, project, or API? Use your analytics tool to see what's
being searched, browse online community forums and discussion groups, and conduct user research and usability
testing. Make sure you also know your product and how to easily explain user questions, new features, and workflows.

2. Just start
Clearly state what's going to be covered in your documentation and why it should be valuable to the
reader to kick it off.

3. Capture the deets
Create an outline and draft up your content. Write with an appropriate voice and tone (be human!)
for your audience and be consistent and concise with your language. Communicate important details clearly.

4. Format
Organize your page so it's easy to follow from beginning to end. Cut out fluff and break up long content
with visuals like diagrams, screenshots, and images.

5. Review
Get feedback on your their vs. there and to vs. too's. Ensure reviewers understand the goal of your documentation.
This will help uncover confusing language or point out missing steps.

6. Publish
After revisions and edits, you're ready to go live! Publish your work and keep your ear to the ground for any
feedback and comments. Documentation isn't something to set and forget!




- TIPS -




Keep it brief
 - Your documentation should have just the right amount of information to get the job done without
   raising a help ticket. Provide key points and the option for readers to go into further detail.

Visuals are key
 - Visuals are key to comprehension. Product design, code samples, in-product demos, screenshots,
   and video tutorials play a large role in helping a reader to fully understand the concept,
   the how-to, or the to-do. Also keep in mind the layout, legibility, and easily digestible chunks.

Know your audience
 - Take a walk in the user's shoes. Know your reader, take a stroll through their user journey of your
   product and your documentation. This should always guide how and what you write.




- POTENTIAL DOCUMENTATION FRAMEWORKS -




Hexo (What we currently use)
  - Its free
  - Easy to use
  - Looks decent (Can be customized with sass)
  - Automated deployment already setup

Gitbook (offers these extra features)
  - Google Analytics
    - Leverage Google Analytics to measure & understand your traffic.
  - Insights
    - Track your traffic, ratings, and content quality.
  - GitHub Sync
    - Keep your workflow and sync your docs with GitHub.
  - Fast Search
    - Find exactly what you're looking for in seconds
  - Branding
    - Custom domains and custom branding (logos, colors, headers ...)

Free version gives you
  - 10 spaces
  - Custom domain

$40 a month plan gets you these features
  - Unlimited spaces
  - Custom domains
  - Powerful search
  - Usage analytics
  - Import external content

$300 a month gets you these features
  - Shareable links (Not sure what they mean by this)
  - Advanced branding
  - PDF Export
  - SAML single sign-on (SSO)
  - Organization domains (???)



Thoughts...



As long as we can host everything we have right now using gitbook for free, I think it would be worth
switching over rather than using hexo. Being able to grab insights on user interaction with the docs seems
like it might be a valuable thing to have in the future. If we can't host everything we already have on gitbook
using the free version for whatever reason, continue using hexo for now and maybe switch over if gain a massive
influx of users. Hexo will be fine to use for now, but Gitbook looks a bit more professional and provides a lot
of extra features that could be useful. Because software documentation can sometimes act as the frontpage of a
project (from the perspective of a dev), this can essentially make or break user interest.




-------------------------------------------

Documentation psuedo format/structure

(Everything in parenthesis are my thoughts and considerations on what should go in those sections)

-------------------------------------------




INSTALLATION
  - Setup
  - Quickstart
  - Getting started

EXAMPLE APPLICATIONS
  - coin-app-template (Link to repo, and step-by-step guide on creating it from scratch)
  - chat-app-template (Link to repo, and step-by-step guide on creating it from scratch)
  - liberdus (When it's complete)
  - DEX-template (Might be a cool example to build that stresses some more advanced transaction logic)

TOOLS
  - Shardus CLI tool
    - init
    - network
    - scan
    - debug
    - deploy
  - Monitor Server
  - Monitor Client
  - Archive Server (More of an advanced tool, users shouldn't have to know how to use this)
  - Crypto Utils (Documentation for all the crypto utility functions)
  - Crypto Web (Web version of crypto-utils)

MAIN CONCEPTS
  - Development (3 main concepts are different account types, transactions that effect them, and API's to query them)
    - Initialization (Explain everything shardus needs in order to get a barebones server up and running)
    - Accounts (Explain how every bit of data that exists, exists in the form of an account and how to work with that)
    - Transactions (Explain the flow process that a transaction goes through and the setup functions that need modification to support new transactions)
      - User transactions ("transfer", "message"... Transactions that users submit to the network)
      - Node induced transactions ("node_reward", "tally_votes"... Transactions that nodes are responsible for submitting)
    - Application API (Maybe just API? Explain that the app developer is responsible for creating their own API for things shardus isn't aware of)
    - CLI (Explain that developing and testing with a custom CLI tool makes life a lot easier)
    - Frontend (Completely upto the developer, shardus is not required for doing this, crypto-web tool must be used)
  - Security
    - Validating transaction data (User sending more coins than they have)
    - Ensuring data cannot be manipulated (Only proposal with the "winner" flag set to true will pass validation for "apply_parameters")
  - Testing (Explain how to test dapps, what to look for, and what to stress)
    - Single node
    - Network of nodes
    - Spamming
  - Debugging
    - State (Explain state related bugs that could cause network to get out of sync, and how to solve them)
      - Doing state modification outside of the "apply" function
      - Using global accounts for synchronizing application state across nodes
    - Transaction (Explain transaction related bugs that could cause shardus to fail, and how to solve them)
      - Passing duplicate keys into a transaction
      - Forgetting to pass in a key
      - Generating the wrong account type
    - Application (Explain Bugs that are caused by the application, not shardus)
      - Failure to ensure a node transaction occured when it needed to (Generating an issue)
    - Common Errors (Common errors that we've come across)
      - testAccountTime failed
      - N redundant responses
      - Network timeout
  - Deployment
    - Remote server (Like a linode server for example)
    - AWS (See advanced guide? Or just explain it here?)
  - Considerations
    - Incentivize users
    - Incentivize miners

ADVANCED GUIDES
  - Advanced Configuration (Things people can play around with when we go open source?)
  - Rate Limiting (This could maybe go under MAIN CONCEPTS/development, but we should explain this somewhere)
  - Production dapp (Outline what a production dapp should look like I guess? Or the boxes that need to be checked in order to call it "production")
  - Deployment to AWS
  - Automated CI/CD testing
  - Listing on DEX (uniswap?)
  - Airdrop for ERC-20 holders
  - Cross chain atomic swaps

API REFERENCE
  - Configuration
    - Server
    - Logs
    - Storage
  - Interface
    - constructor (dapp = shardus())
    - setup
      - sync
      - validateTxnFields
      - apply
      - getKeyFromTransaction
      - getStateId
      - deleteLocalAccountData
      - setAccountData
      - getRelevantData
      - updateAccountFull
      - updateAccountPartial
      - getAccountDataByRange
      - getAccountData
      - getAccountDataByList
      - calculateAccountHash
      - resetAccountData
      - deleteAccountData
      - getAccountDebugValue
      - canDebugDropTx
      - close
    - start
    - registerExternalGet
    - registerExternalPost
    - registerExceptionHandler
    - applyResponseAddState
    - createApplyResponse
    - getNodeId
    - getNode
    - getClosestNodes
    - getLocalOrRemoteAccount
    - getRemoteAccount
    - getLatestCycles
    - log
    - put
    - set
    - p2p (Might want shardus to expose these rather than shardus.p2p)
      - isFirstSeed
      - on (Callback for events like "joining" "syncing" and "active")

CONTRIBUTING
  - Apply to be a dev
  - Support the project
  - Provide feedback
  - Raise an issue

FAQ
  - What is shardus?
  - Why use shardus over (blah blah blah)?
  - What do I need to know in order to build with shardus?
  - How many transaction can X amount of nodes handle per second with X amount of nodes per shard?
  - Etc...