# Getting Started

## Welcome to the Shardus Developer Docs!

[__Shardus__](https://shardus.com) is a Node.js framework for building
decentralized applications that scale.

## Try Shardus

Whether you're building a full-fledged decentralized application, or just want
to get an idea of what's possible with Shardus, the links in this section will
help you get going.

### Quickstart

Shardus has been designed from the ground up to make decentralized app
development as painless as possible. The fastest way to see how Shardus does
this is to jump right in and try it with the five minute [quickstart
guide](installation/quickstart.html).

### Shardus DB

One of the amazing shardus developers has developed a tool called
[shardusDB](https://gitlab.com/shardus/applications/shardus-db), which wraps
the more complex low level shardus interface with a simple, trivial to use
abstraction. This allows dapp developers to develop dapps without having to
learn the more challenging underlying shardus code, but still get the
benefits of developing on the shardus network.

Take a look at a simple game of [tic tac
toe](examples/tic-tac-toe-template.html) written using `shardusDB`. Here's its
[soure code](https://gitlab.com/shardus/applications/tic-tac-toe).

### Application Templates

Another great way to start building applications with Shardus is to use an
existing application template. A list of application templates can be found on
the Shardus Gitlab page [here](https://gitlab.com/shardus/applications).

## Learn Shardus

The links in this section provide more detail on:

* [The Shardus SDK Tools for app development.](./tools)
* [Core Concepts for Shardus Apps](./main-concepts)
* [The Shardus Application API](./api)

### Main Concepts

---

* [Development](main-concepts/development)
* [Security](main-concepts/security)
* [Testing](main-concepts/testing)
* [Debugging](main-concepts/debugging)
* [Deployment](main-concepts/deployment)
* [Considerations](main-concepts/considerations)

---

### SDK Reference

---

* [Shardus API Interface](api/interface)
* [Configuration Parameters](api/configuration)
