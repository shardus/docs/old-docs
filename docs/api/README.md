# API Reference

This section provides the documentation for all the available methods and configuration options within Shardus.

[Configuration](./configuration) Will tell you all about the available configuration options for your application.

[Interface](./interface) Will tell you all about the available methods shardus exposes to your application.
